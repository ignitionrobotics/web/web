#!/bin/bash

# export API_HOST="https://api.ignitionrobotics.org"
export API_HOST="http://localhost:8000"
export API_VERSION="1.0"
export APP_HOST="https://staging-app.ignitionrobotics.org"
export AUTH0_CLIENT_ID="RWL4naBT7MGF8kZ89rrNc1ivDho25UAU"
export AUTH0_CLIENT_DOMAIN="osrf.auth0.com"
export AUTH0_AUDIENCE="https://ignitionrobotics.org"
export AUTH0_REDIRECT="http://localhost:3000/callback"
