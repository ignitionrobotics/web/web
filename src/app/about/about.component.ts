import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { markParentViewsForCheckProjectedViews } from '@angular/core/src/view/util';

@Component({
  selector: 'ign-about',
  templateUrl: 'about.component.html',
  styleUrls: ['about.component.scss']
})

export class AboutComponent implements OnInit {

  constructor(private titleService: Title, private meta: Meta) {
  }

  public ngOnInit(): void {
    this.titleService.setTitle('About -- Ignition');
    this.meta.updateTag({name: 'title', content: 'About -- Ignition'});
    this.meta.updateTag({name: 'description',
      content: 'What is Ignition, its history, and relation to Gazebo.'});
  }
}
