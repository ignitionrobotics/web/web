import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { LibsService } from './libs.service';

@Component({
  selector: 'ign-libs',
  templateUrl: 'libs.component.html',
  styleUrls: ['libs.component.scss']
})

export class LibsComponent implements OnInit  {
  constructor(public libsService: LibsService,
              private titleService: Title,
              private meta: Meta) {
  }

  public ngOnInit(): void {
    this.titleService.setTitle('Libs -- Ignition');
    this.meta.updateTag({name: 'title', content: 'Libs -- Ignition'});
    this.meta.updateTag({name: 'description',
      content: 'Libraries available through Ignition.'});
  }
}
